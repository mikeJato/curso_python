# -*- coding: utf-8 -*-

def decorador (func):
    
    def funcion_base(password):
        
        if password == 'platzi':
            return func()
        else:
            print('La contraseña es incorrecta')

    return funcion_base
    
@decorador
def protected_func():
    print('Tu contraseña es correcta.')

if __name__ == '__main__':
    password = str(raw_input('ingresa tu contraseña: '))
    protected_func(password)